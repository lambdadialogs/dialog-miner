mine-expr.rkt: the dialog miner; contains the following three sections, each
identified by a comment header in the code:

   * a suite of helper/utility functions,

   * functions used to mine expressions, including mine-expr,

   * a test suite for mining expressions;
     a variety of examples of dialog mining from the paper
     (i.e., invocations of the mine-expr function).

To run, open mine-expr.rkt in Dr.Racket (v.5.3) and click the "Run" button in
the top right corner of the IDE window.
