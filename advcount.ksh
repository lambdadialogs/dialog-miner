#!/usr/bin/env ksh

# $1 input file

# run as
# $ ./advcount.ksh coffeeFreqs.txt  | sort -rn -k 5 > coffeeCompression.txt

for incount in 1 2 3 4 5 6 7 8 9 10 11 12 13; do
   for outcount in 1 2 3 4 5 6 7; do
      if (( $incount >= $outcount )); then
         ratio=$(echo a | awk 'BEGIN { printf ("%f", (float) ('$incount' - '$outcount') / '$incount') } { }')
         (( ratio *= 100 ))
      fi
      count=$(grep "^$incount $outcount$" $1 | wc -l)
      if (( count != 0 )); then
         #printf "%2d -> %d (%.2f): %3d\n" $incount $outcount $ratio $count
         printf "%2d -> %d ( %2d \% ): %3d\n" $incount $outcount $ratio $count
      fi
   done
done

exit

