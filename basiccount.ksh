#!/usr/bin/env ksh

# $1 input file

#print Ones: $(grep "^\'[(]1" $1 | wc -l)
#print Twos: $(grep "^\'[(]2" $1 | wc -l)
#print Threes: $(grep "^\'[(]3" $1 | wc -l)
#print Fours: $(grep "^\'[(]4" $1 | wc -l)
#print Fives: $(grep "^\'[(]5" $1 | wc -l)
#print Sixes: $(grep "^\'[(]6" $1 | wc -l)
#print Sevens: $(grep "^\'[(]7" $1 | wc -l)

print "1,$(grep "[ ]1[ ]" $1 | wc -l)"
print "2,$(grep "[ ]2[ ]" $1 | wc -l)"
print "3,$(grep "[ ]3[ ]" $1 | wc -l)"
print "4,$(grep "[ ]4[ ]" $1 | wc -l)"
print "5,$(grep "[ ]5[ ]" $1 | wc -l)"
print "6,$(grep "[ ]6[ ]" $1 | wc -l)"
print "7,$(grep "[ ]7[ ]" $1 | wc -l)"
print "8,$(grep "[ ]8[ ]" $1 | wc -l)"
print "9,$(grep "[ ]9[ ]" $1 | wc -l)"
print "10,$(grep "[ ]10[ ]" $1 | wc -l)"
print "11,$(grep "[ ]11[ ]" $1 | wc -l)"
print "12,$(grep "[ ]12[ ]" $1 | wc -l)"
print "13,$(grep "[ ]13[ ]" $1 | wc -l)"

print "No compression"

print "1,$(grep "[(]1[ ]1[ ][(]" $1 | wc -l)"
print "2,$(grep "[(]2[ ]2[ ][(]" $1 | wc -l)"
print "3,$(grep "[(]3[ ]3[ ][(]" $1 | wc -l)"
print "4,$(grep "[(]4[ ]4[ ][(]" $1 | wc -l)"
print "5,$(grep "[(]5[ ]5[ ][(]" $1 | wc -l)"
print "6,$(grep "[(]6[ ]6[ ][(]" $1 | wc -l)"
print "7,$(grep "[(]7[ ]7[ ][(]" $1 | wc -l)"
print "8,$(grep "[(]8[ ]8[ ][(]" $1 | wc -l)"
print "9,$(grep "[(]9[ ]9[ ][(]" $1 | wc -l)"
print "10,$(grep "[(]10[ ]10[ ][(]" $1 | wc -l)"
print "11,$(grep "[(]10[ ]11[ ][(]" $1 | wc -l)"
print "12,$(grep "[(]10[ ]12[ ][(]" $1 | wc -l)"
print "13,$(grep "[(]10[ ]13[ ][(]" $1 | wc -l)"

exit
